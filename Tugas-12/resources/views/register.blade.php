<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="welcome">
      @csrf
      <label>First Name</label><br>
      <input type="text" name="firstname" /><br><br>
      <label>Last Name</label><br>
      <input type="text" name="lastname" /><br><br>

      <label>Gender</label><br><br>
      <input type="radio" />Male<br />
      <input type="radio" />Female<br />
      <input type="radio" />Other<br /><br>

      <label>Nationality</label><br><br>
      <select>
        <option value="actual value 1">Indonesian</option>
        <option value="actual value 2">American</option>
        <option value="actual value 3">Malaysian</option>
      </select><br><br>

      <label>Language Spoken</label><br><br>
      <input type="checkbox">Bahasa Indonesia<br>
      <input type="checkbox">English<br>
      <input type="checkbox">Other<br><br>

      <label>Bio:</label><br><br>
      <textarea name="bio" cols="30" rows="10"></textarea><br>

      <input type="submit" value="Sign Up">
    </form>
  </body>
</html>
